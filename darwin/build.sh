#!/bin/sh

echo "Build FFmpeg for OSX"
./build_osx.sh

echo "Build FFmpeg for iOS"
./build_ios.sh

echo "Create FFmpeg for OSX Universal"
./build_osx.sh lipo

echo "Create FFmpeg for iOS Universal"
./build_ios.sh lipo
