﻿MInGW 에서 32비트 버전과 64비트 버전을 모두 빌드하고 설치한다 (make install)

MInGW 의 루트 디렉토리에 ffmpeg32, ffmpeg64 디렉토리를 통째로 여기에 복사한다.

그후 mingw_base 를 빌드하면 ffmpeg32/lib, ffmpeg64/lib 에 mingw_base.lib 가 생성된다.

libgcc.a 와 libmingwex.a 대신 mingw_base.lib 를 사용하여야 한다.

# lib 파일 복사 (mingw 에서 복사)
mingw_base/CopyLib_FromMinGWL.bat 를 실행하면 mingw 디렉토리에서 .a 파일들을 lib,lib_org에 복사한다.

# lib 파일 복사 (로컬에 있는거 복사)
mingw_base/CopyLib.bat 을 실행하면 lib_org 에서 .a 파일들을 복사한다

# 오류 심볼 제거
mingw_base/RemoveSymbol32.bat 를 실행하면 32비트 버전 .a 파일에서 오류난 심볼을 제거
mingw_base/RemoveSymbol64.bat 를 실행하면 64비트 버전 .a 파일에서 오류난 심볼을 제거

# mingw32 를 새로 업데이트 한 경우
libgcc.a 와 libmingwex.a 파일을 mingw_base/lib, mingw_base/lib_org 디렉토리에 복사하고 (또는 CopyLib_FromMinGW.bat 파일 수정해야함)
플랫폼에 따라 아래와 같이 파일명을 변경해야 한다.
libgcc.a -> libgcc32.a, libgcc64.a
libmingwex.a -> libmingwex32.a, libmingwx64.a
