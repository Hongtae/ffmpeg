///////////////////////////////////////////////////////////////////////////////
// mingw_base
//
// mingw32/64 를 이용해서 빌드한 ffmpeg 와 링크하기 위한 static-library
//
// libgcc.a 와 libmingwex.a 에 링크 문제가 있어서 mingw_base.lib 으로 래핑한다.
//
// 개별적으로 빌드한 후 mingw_base.lib 파일을 ffmpeg lib 파일들과 함께 링크한다.
//
// 1. 문제가 있는 라이브러리는 lib.exe 를 이용하여 해당 오브젝트를 제거해야한다.
//
// 예:
//	lib /remove:오브젝트 libmingwex.a
//
// 2. 또한 없는 함수에 대해서는 여기서 추가한다.
//
// 3. 라이브러리 링크는 #pragma 를 사용하지 않고 프로젝트 세팅에서 dependencies 로 직접 넣어야한다.
//
///////////////////////////////////////////////////////////////////////////////

#include <math.h>
#include <windows.h>

#define GAI_STRERROR_BUFFER_SIZE 1024

char* gai_strerrorA(int ecode)
{
    DWORD dwMsgLen;
    static char buff[GAI_STRERROR_BUFFER_SIZE + 1];

    dwMsgLen = FormatMessageA(FORMAT_MESSAGE_FROM_SYSTEM
                             |FORMAT_MESSAGE_IGNORE_INSERTS
                             |FORMAT_MESSAGE_MAX_WIDTH_MASK,
                              NULL,
                              ecode,
                              MAKELANGID(LANG_NEUTRAL, SUBLANG_DEFAULT),
                              (LPSTR)buff,
                              GAI_STRERROR_BUFFER_SIZE,
                              NULL);

    return buff;
}

