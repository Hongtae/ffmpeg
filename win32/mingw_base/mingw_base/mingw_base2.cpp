﻿#include <math.h>
#include <windows.h>

// 인라인함수들을 링크하기 위한 더미 함수
void _initTrigonometricFunctions(float v, float* p)
{
	// 삼각함수 라이브러리 초기화 (한번 써줘야 심볼 생긴다)
	float tmp[] = {
		sin(v), cos(v), tan(v),
		sinh(v), cosh(v), tanh(v),
		asin(v), acos(v), atan(v),
		atan2(v, v),
		log10f(v), log2f(v), log2(v)
	};
	memcpy(p, tmp, sizeof(tmp));
}
