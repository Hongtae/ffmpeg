#!/bin/sh

if [ -z $NDK_ROOT ]
then
	echo "ndk-root not set?"
	exit;
fi

if [ ! -d $NDK_ROOT ]
then
	echo "NDK_ROOT not directory?"
	exit
fi

echo "NDK_ROOT = $NDK_ROOT"

PLATFORM=$NDK_ROOT/platforms/android-14/arch-arm
TOOLCHAIN_BIN=$NDK_ROOT/toolchains/arm-linux-androideabi-4.8/prebuilt/darwin-x86_64/bin

CWD=`pwd`
SOURCE="ffmpeg"
SCRATCH="scratch"
PREFIX=`pwd`/build

mkdir -p "$SCRATCH"
cd "$SCRATCH"

$CWD/$SOURCE/configure \
 --disable-doc \
 --disable-debug \
 --disable-encoders \
 --disable-ffplay \
 --disable-ffserver \
 --disable-ffmpeg \
 --disable-ffprobe \
 --disable-devices \
 --enable-cross-compile \
 --enable-runtime-cpudetect \
 --target-os=linux \
 --arch=arm \
 --cc=$TOOLCHAIN_BIN/arm-linux-androideabi-gcc \
 --cross-prefix=$TOOLCHAIN_BIN/arm-linux-androideabi- \
 --nm=$TOOLCHAIN_BIN/arm-linux-androideabi-nm \
 --sysroot=$PLATFORM \
 --extra-cflags="-fPIC -DANDROID -D__thumb__ -mthumb -Wfatal-errors -Wno-deprecated -mfloat-abi=softfp -mfpu=neon -marm -march=armv7-a" \
 --prefix=$PREFIX

make -j3 install
cd $CWD
